package com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.utils.TestBase;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class Actions {

    WebDriver driver;
    TestBase testBase;

    public Actions(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(how = How.ID, using = "email")
    WebElement email;
    @FindBy(how = How.ID, using = "passwd")
    WebElement senha;
    @FindBy(how = How.ID, using = "SubmitLogin")
    WebElement enviar;
    @FindBy(how = How.CLASS_NAME, using = "login")
    WebElement btnentrar;
    @FindBy(how = How.ID, using = "search_query_top")
    WebElement buscar;
    @FindBy(how = How.NAME, using = "submit_search")
    WebElement btnbuscar;
    @FindBy(how = How.CSS, using = "a.button.lnk_view.btn.btn-default")
    WebElement detalhes;
    @FindBy(how = How.NAME, using = "Green")
    WebElement cor;
    @FindBy(how = How.NAME, using = "Submit")
    WebElement adicionar;
    @FindBy(how = How.CSS, using = "div.button-container > span")
    WebElement continue_compra;
    @FindBy(how = How.ID, using = "group_1")
    WebElement escolher_tamanho;
    @FindBy(how = How.CLASS_NAME, using = "icon-plus")
    WebElement mais_um;
    @FindBy(how = How.CSS, using = "div.button-container > a")
    WebElement checkout;
    @FindBy(how = How.CSS, using = "a.button.btn.btn-default.standard-checkout.button-medium")
    WebElement proceed_checkout;
    @FindBy(how = How.NAME, using = "processAddress")
    WebElement endereco_continuar;
    @FindBy(how = How.NAME, using = "processCarrier")
    WebElement envio_continuar;
    @FindBy(how = How.ID, using = "cgv")
    WebElement aceite_termo;
    @FindBy(how = How.CLASS_NAME, using = "bankwire")
    WebElement pagamento;
    @FindBy(how = How.CSS, using = "#cart_navigation > button")
    WebElement confirmo_pedido;
    @FindBy(how = How.CSS, using = "#center_column > p > a")
    WebElement voltar_pedidos;
    @FindBy(how = How.CLASS_NAME, using = "price")
    WebElement preco;
    @FindBy(how = How.CLASS_NAME, using = "price")
    WebElement preco_total;
    @FindBy(how = How.CLASS_NAME, using = "footable-toggle")
    WebElement plus;
    @FindBy(how = How.XPATH, using = "/html/body/div/div[2]/div/div[3]/div/div/table/tbody/tr[2]/td/div/div[3]/div[2]/a[1]")
    WebElement details;
    @FindBy(how = How.XPATH, using = "//*[@id=\"order-detail-content\"]/table/tbody/tr[1]/td[2]/label")
    WebElement pedido1;
    @FindBy(how = How.XPATH, using = "//*[@id=\"order-detail-content\"]/table/tbody/tr[2]/td[2]/label")
    WebElement pedido2;
    @FindBy(how = How.XPATH, using = "//*[@id=\"order-detail-content\"]/table/tbody/tr[3]/td[2]/label")
    WebElement pedido3;

    public void Clicar_login() {
        int timeout = 10;
        new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOf(btnentrar));
        btnentrar.click();
    }

    public void FazerLogin(String strEmail, String strSenha){
        email.sendKeys(strEmail);
        senha.sendKeys(strSenha);
        enviar.click();
    }

    public void AdicionandoProduto1(String Produto1){
        buscar.sendKeys(Produto1);
        btnbuscar.click();
        detalhes.click();
        cor.click();
        adicionar.click();
        continue_compra.click();
        buscar.clear();
    }

    public void AdicionandoProduto2(String Produto2){
        buscar.sendKeys(Produto2);
        btnbuscar.click();
        detalhes.click();
        Select tamanho = new Select(escolher_tamanho);
        tamanho.selectByVisibleText("M");
        adicionar.click();
        continue_compra.click();
        buscar.clear();
    }

    public void AdicionandoProduto3(String Produto3){
        buscar.sendKeys(Produto3);
        btnbuscar.click();
        detalhes.click();
        mais_um.click();
        adicionar.click();
        checkout.click();
    }

    public void FinalizandoPedido(){
        proceed_checkout.click();
        endereco_continuar.click();
        aceite_termo.click();
        envio_continuar.click();
        pagamento.click();
        confirmo_pedido.click();

    }

    public void VerificandoPedido(){
        preco.getText();
        voltar_pedidos.click();
        preco_total.getText();
        assertEquals(preco, preco_total);
        plus.click();
        details.click();
    }

    public void ValidarPedido(){
        assertEquals(pedido1.getText(), "Printed Chiffon Dress - Color : Green, Size : S");
        assertEquals(pedido2.getText(), "Faded Short Sleeve T-shirts - Color : Orange, Size : M");
        assertEquals(pedido3.getText(), "Blouse - Color : Black, Size : S");
    }

}

