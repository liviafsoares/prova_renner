package com.steps;

import com.utils.TestBase;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import com.pages.Actions;

public class SearchTest {
        WebDriver driver;

    @Dado("^que estou logado na pagina automationpractice$")
    public void que_acesso_a_pagina() {
        TestBase.Iniciar();
        Actions Login = PageFactory.initElements(TestBase.Iniciar(), Actions.class);
        Login.Clicar_login();
        Login.FazerLogin("teste896198619@teste.com", "12345");
    }
    @Quando("^finalizo uma compra$")
    public void pesquiso_veiculo_por_marca_e_modelo() {
        Actions Login = PageFactory.initElements(TestBase.Iniciar(), Actions.class);
        Login.AdicionandoProduto1("Printed Chiffon Dress");
        Login.AdicionandoProduto2("Faded Short Sleeve T-shirts");
        Login.AdicionandoProduto3("Blouse");
        Login.FinalizandoPedido();
    }
    @Entao("^o pedido deve ser exibido no historico$")
    public void deve_retornar_todos_carros() {
        Actions Login = PageFactory.initElements(TestBase.Iniciar(), Actions.class);
        Login.VerificandoPedido();
        Login.ValidarPedido();
        driver.close();
    }

}