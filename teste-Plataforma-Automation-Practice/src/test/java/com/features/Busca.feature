# language: pt

Funcionalidade: Teste de plataforma

  Cenário: Finalizar compra e verificar histórico
    Dado que estou logado na pagina automationpractice
    Quando finalizo uma compra
    Então o pedido deve ser exibido no historico
