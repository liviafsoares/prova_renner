package com.automationPracticeApi.Base;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import org.hamcrest.Matchers;


public class BaseTeste implements Constantes{

    public static void TestBase(){
        RestAssured.baseURI = BASE_URL;
        RestAssured.port = PORT;
        RestAssured.basePath = BASE_PATH;
    }

}
