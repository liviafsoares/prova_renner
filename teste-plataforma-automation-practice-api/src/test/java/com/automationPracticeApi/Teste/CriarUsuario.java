package com.automationPracticeApi.Teste;

import com.automationPracticeApi.Base.BaseTeste;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class CriarUsuario extends BaseTeste {
    @Before
    public void inicio(){
        BaseTeste.TestBase();
    }


    @Test
    public void CriarNovoUsuario() {
        given()
                .body("{\n" +
                        "    \"name\": \"Livia\",\n" +
                        "    \"job\": \"Teste\"\n" +
                        "}")
                .contentType(ContentType.JSON)
        .when()
                .post("/api/users")
        .then()
                .log().all()
                .body("name", is("Livia"),
                "job", is("Teste"))
                .statusCode(HttpStatus.SC_CREATED);

    }

}
