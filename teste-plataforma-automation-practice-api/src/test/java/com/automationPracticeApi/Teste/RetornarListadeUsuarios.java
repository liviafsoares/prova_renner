package com.automationPracticeApi.Teste;

import com.automationPracticeApi.Base.BaseTeste;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class RetornarListadeUsuarios {
        @Before
        public void inicio() {
            BaseTeste.TestBase();
        }

        @Test
        public void DeveRetornarListaDeUsuarios() {
            given()
                    .contentType(ContentType.JSON)
            .when()
                    .get("/api/users?page=2")
            .then()
                    .log().all()
                    .body("page", is(2),
                            "per_page", is(6),
                            "total", is(12),
                            "total_pages", is(2),
                            "data[0].id", is(7),
                            "data[0].email", is("michael.lawson@reqres.in"),
                            "data[0].first_name", is("Michael"),
                            "data[0].last_name", is("Lawson"),
                            "data[0].avatar", is("https://reqres.in/img/faces/7-image.jpg"),
                            "data[5].id", is(12),
                            "data[5].email", is("rachel.howell@reqres.in"),
                            "data[5].first_name", is("Rachel"),
                            "data[5].last_name", is("Howell"),
                            "data[5].avatar", is("https://reqres.in/img/faces/12-image.jpg"),
                            "support.url", is("https://reqres.in/#support-heading"),
                            "support.text", is("To keep ReqRes free, contributions towards server costs are appreciated!"))
                    .statusCode(HttpStatus.SC_OK);


        }
}
