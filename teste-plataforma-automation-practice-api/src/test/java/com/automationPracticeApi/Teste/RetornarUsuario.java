package com.automationPracticeApi.Teste;
import com.automationPracticeApi.Base.BaseTeste;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class RetornarUsuario {
    @Before
    public void inicio(){
        BaseTeste.TestBase();
    }

    @Test
    public void DeveRetornarUsuarioPorId() {
        given()
                .contentType(ContentType.JSON)
        .when()
                .get("/api/users/2")
        .then()
                .log().all()
                .body("data.id", is(2),
                        "data.email", is("janet.weaver@reqres.in"),
                        "data.first_name", is("Janet"),
                        "data.last_name", is("Weaver"),
                        "data.avatar", is("https://reqres.in/img/faces/2-image.jpg"),
                        "support.url", is("https://reqres.in/#support-heading"),
                        "support.text", is("To keep ReqRes free, contributions towards server costs are appreciated!"))
                .statusCode(HttpStatus.SC_OK);

    }
}
